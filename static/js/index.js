var Icon = React.createClass({
    render: function() {
        var className = "fa fa-" + this.props.icon + " fa-fw";
        return (
            <i className={className}></i>
        );
    }
});

var NodeHeader = React.createClass({
    render: function() {
        return (
            <div onClick={this.props.click} className="nodeTitle"> <Icon icon={this.props.icon}/> {this.props.title} </div>
        );
    }
});

var TreeNode = React.createClass({

    getInitialState: function() {
        return {visible: true};
    },

    visibilityClick: function() {
        var current = this.state.visible;
        this.setState({visible: !current});
    },

    render: function() {
        var childNodes = [];
        if(this.props.children && this.state.visible) {
            childNodes = this.props.children.map(function (node) {
                return (
                    <TreeNode key={node.id} title={node.title} children={node.children} />
                );
            });
        }

        var icon = "caret-right";
        if(this.state.visible) {
            icon = "caret-down"
        }

        return (
            <div className="treeNode">
                <NodeHeader title={this.props.title} click={this.visibilityClick} icon={icon} />
                {childNodes}
            </div>
        );

    }
});

var TreeDisplay = React.createClass({
    getInitialState: function() {
        return {data: {}};
    },

    componentDidMount: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    render: function() {
        return (
            <div className="treeDisplay">
                <TreeNode id={this.state.data.id} title={this.state.data.title} children={this.state.data.children} />
            </div>
        );
    }
});

React.render(
    <TreeDisplay url="data/simpleData.json" />,
    document.getElementById('tree')
);